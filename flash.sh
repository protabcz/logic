#!/bin/sh
port=/dev/ttyUSB0
if [ -n "$1" ]; then
	port=$1
fi
esptool.py --chip esp32s3 --port "$port" erase_flash
esptool.py --chip esp32s3 --port "$port" write_flash -z 0 firmware.bin
