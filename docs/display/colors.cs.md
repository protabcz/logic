# Barvy

Kdekoliv je potřeba zadat barvu, je možno použít:

* Řetězec s anglickým názvem barvy, např. `#!py "blue"`. Je možno použít
  libovolnou z [HTML barev](https://www.w3schools.com/tags/ref_colornames.asp).

* Řetězec v `#!py "#RGB"` nebo `#!py "#RRGGBB"` formátu.

* Trojici hodnot `(r, g, b)`, kde jednotlivé barevné složky jsou buď
  desetinná čísla od 0 do 1, nebo celá čísla od 0 do 255.

## Pokročilá práce s barvami

Logic nepracuje s barvami jako s řetězci. Pokud zadáte barvu jako řetězec
(nebo jako trojici hodnot), musí ji nejprve převést na číslo, se kterým umí
pracovat.

Neustálé převádění barev z řetězce nebo trojice hodnot může program zbytečně
zpomalovat. V takovém případě lze převést barvu jen jednou, tuto převedenou
hodnotu si zapamatovat, a pak ji opakovaně používat. K tomu slouží funkce 
`#!py Color.translate(color)`.

!!! example "Příklad"

    ```py
    from logic import *

    my_color = Color.translate("green")
    display.set_pixel(1, 8, my_color)
    display.set_pixel(2, 7, my_color)
    ```
