# Displej

Displej se ovládá přes modul `display`. Dostupné jsou následující základní
funkce:

## `#!py display.set_pixel(x, y, color)`

Rozsvítí bod na souřadnicích `x`, `y` danou barvou (`color`). Souřadnice
`x` a `y` se počítají od nuly, levý horní roh má souřadnice 0, 0.

!!! example "Příklad"

    ```py
    from logic import *

    display.set_pixel(4, 4, "green")
    ```

Jako barvu je možno použít řetězec s anglickým názvem barvy. Další možnosti
jsou vysvětleny [v kapitole barvy](colors.md).

Pro zhasnutí bodu jej stačí „rozsvítit“ černou, např.
`#!py display.set_pixel(2, 3, "black")`.

## `#!py display.clear()`

Smaže displej, tedy zhasne všechny body.

## `#!py display.load(path)`

Zobrazí obrázek ze souboru. Soubor musí být nejdříve nahrán do Logicu;
k tomu použijte tlačítko *Files* v Mu.

Soubor musí být ve formátu BMP, musí mít rozměr 10×10 pixelů a musí mít
24bitové barvy.

## `#!py display.status.set_pixel(x, color)`

Rozsvítí bod ve stavovém řádku nad displejem danou [barvou](colors.md)
(`color`). Akceptuje pouze jedinou souřadnici (`x`), která se počítá od
nuly.

## `#!py display.status.clear()`

Smaže stavový řádek, tedy zhasne všechny body.

## Pokročilé funkce

Jsou určeny pro rychlejší práci s displejem. Jejich popis je
[v samostatné kapitole](advanced.md).
