# Pokročilý displej

Základem pokročilé práce s displejem je samostatný obrázek, `Image`. Obrázek
lze upravovat, a potom rychle překopírovat na displej.

## `#!py Image()`

Vytvoří nový obrázek. Všechny body budou černé.

!!! example "Příklad"

    ```py
    from logic import *

    my_image = Image()
    ```

Lze také předat dva parametry, šířku a výšku. To je užitečné, pokud chceme
vytvořit obrázek pro stavový řádek: `#!py my_status = Image(5, 1)`.

## `#!py Image.set_pixel(x, y, color)`

Nastaví bod obrázku na souřadnicích `x`, `y` na danou
[barvu](index.md#barvy) (`color`). Souřadnice jsou počítány od nuly, levý
horní bod obrázku má souřadnice 0, 0.

!!! example "Příklad"

    ```py
    from logic import *

    my_image = Image()
    my_image.set_pixel(0, 0, "red")
    ```

## `#!py Image.clear()`

Smaže obrázek, tedy nastaví všechny jeho body na černou.

## `#!py Image.scroll(x_step, y_step)`

Posune obrázek daným směrem. Je-li např. `x_step` rovno 1, posune všechny
body obrázku o 1 doprava. Je-li `y_step` rovno -1, posune všechny body
obrázku o 1 nahoru.

Body, které se dostanou mimo obrázek, jsou ztraceny. Body, které „vzniknou“
na druhé straně obrázku, mají nedefinovanou hodnotu, je tedy potřeba je před
zobrazením obrázku na něco nastavit.


!!! example "Příklad"

    ```py
    my_image.scroll(0, 1)
    for x in range(10):
        my_image.set_pixel(x, 0, "black")
    ```

## `#!py Image.load(path)`

Nahraje obrázek ze souboru. Soubor musí být nejdříve nahrán do Logicu;
k tomu použijte tlačítko *Files* v Mu.

Soubor musí být ve formátu BMP, jeho rozměry musí odpovídat rozměrům obrázku
a musí mít 24bitové barvy.

!!! example "Příklad"

    ```py
    from logic import *

    my_image = Image()
    my_image.load('welcome.bmp')
    ```

## `#!py display.show(image)`

Zobrazí daný obrázek (`image`) na displeji.

!!! example "Příklad"

    ```py
    from logic import *

    my_image = Image()
    my_image.load('welcome.bmp')
    my_image.scroll(0, 1)
    for x in range(10):
        my_image.set_pixel(x, 0, "black")
    display.show(my_image)
    ```

## `#!py display.status.show(image)`

Zobrazí daný obrázek (`image`) ve stavovém řádku nad displejem.

## `#!py display.off()`

Vypne napájení displeje včetně stavového řádku nad displejem. Na displej jde
stále kreslit, jen to není vidět.

## `#!py display.on()`

Zapne napájení displeje (a stavového řádku). Vše, co bylo na displeji
zobrazeno před jeho vypnutím a během toho, co byl vypnutý, bude zase vidět.

## `#!py display.image` a `#!py display.commit()`

!!! warning "Pozor"

    Přímé použití interního bufferu nedoporučujeme.

Ve specifických případech lze kreslit přímo do interního bufferu displeje
a tím získat trochu výkonu navíc. Buffer je typu obrázek (`Image`) a je
dostupný jako `#!py display.image`. Pro vynucení vykreslení změn v interním
bufferu je nutné zavolat `#!py display.commit()`.

Obdobně je dostupný `#!py display.status.image` a
`#!py display.status.commit()`.
