# Tlačítka

Zjišťovat stav tlačítek lze pomocí modulů `buttons_a` a `buttons_b`. Pro
levou sadu tlačítek použijte `buttons_a`, pro pravou `buttons_b`.

## Je stisknuté tlačítko?

Každému tlačítku je přiřazena jedna proměnná, ve které je uloženo
`#!py True`, pokud je příslušné tlačíko stisknuto, jinak `#!py False`.
Pro zjištění stavu tlačítka se stačí podívat do této proměnné.

Dostupné proměnné jsou:

* `#!py buttons_a.up` -- tlačítko nahoru
* `#!py buttons_a.down` -- tlačítko dolů
* `#!py buttons_a.left` -- tlačítko vlevo
* `#!py buttons_a.right` -- tlačítko vpravo
* `#!py buttons_a.enter` -- prostřední tlačítko

Obdobně pro `#!py buttons_b`.

!!! example "Příklad"

    ```py
    from logic import *
    import time

    while True:
        if buttons_a.left:
            display.set_pixel(0, 4, 'green')
        else:
            display.set_pixel(0, 4, 'black')
        time.sleep_ms(100)
    ```

## `#!py buttons_a.is_pressed(key)`

Vrátí `#!py True`, pokud je příslušné tlačíko právě teď stisknuto, jinak
`#!py False`. Parametr `button` může být:

* `#!py "up"` -- tlačítko nahoru
* `#!py "down"` -- tlačítko dolů
* `#!py "left"` -- tlačítko vlevo
* `#!py "right"` -- tlačítko vpravo
* `#!py "enter"` -- prostřední tlačítko

Ve většině případů tato funkce není to, co chcete. Může se vám totiž snadno
stát, že vám stisk nějakého tlačítka uteče.

!!! example "Příklad"

    ```py
    from logic import *
    import time

    while True:
        if buttons_a.is_pressed('left'):
            display.set_pixel(0, 4, 'green')
        else:
            display.set_pixel(0, 4, 'black')
        time.sleep_ms(100)
    ```

## `#!py buttons_a.was_pressed(key)`

Vrátí `#!py True`, pokud bylo příslušné tlačíko alespoň jedenkrát stisknuto
od posledního zavolání této funkce, jinak vrátí `#!py False`.

Dokud je tlačítko drženo stisknuté, bude opakované volání `was_pressed`
vracet `#!py True`.

Chování této funkce je stejné jako přímý přístup k proměnným
(`#!py buttons_a.up` atd).

## `#!py buttons_a.clear()`

Zapomene všechna předchozí stisknutá tlačítka.
