# Časovač

Pro nastavení časovačů slouží modul `timer`. Dostupné jsou následující
funkce:

## `#!py timer.create(ms, callback)`

Zavolá danou funkci (`callback`) za daný počet milisekund (`ms`). Vrátí
objekt typu `Alarm`, který může být použit ke zrušení časovače.

Funkce `callback` bude zavolána s jedním parametrem, kterým je objekt typu
`Alarm`. Kromě speciálních případů vám tento parametr moc užitečný nebude;
přesto ho musíte v definici funkce uvést.

Funkce `callback` nemusí být zavolána přesně za daný počet milisekund, může
to být o malinko později. Během běhu funkce `callback` bude hlavní program
pozastaven.

!!! example "Příklad"

    ```py
    from logic import *

    def action(alarm):
        display.set_pixel(4, 4, "red")

    # zavolej funkci action za půl vteřiny:
    timer.create(500, action)
    ```

## `#!py Alarm.stop()`

Předčasně zastaví daný časovač. Pokud už časovač uběhl, nic se nestane.


!!! example "Příklad"

    ```py
    from logic import *

    def action(alarm):
        display.set_pixel(4, 4, "red")

    alarm = timer.create(500, action)
    alarm.stop()
    ```
