# Senzory

## Senzor světla

Funkce `light_level` vrátí aktuální úroveň světla jako celé číslo od 0 do
100. Číslo 100 znamená nejvíce světla, 0 znamená úplnou tmu.

!!! note "Pozor"

    Dejte si pozor na to, že senzor světla je umístěn bezprostředně
    u displeje. Rozsvícení bodů, které jsou blízko senzoru, způsobí, že je
    senzor poměrně intenzivně osvětlen a začne tedy vracet 100.

!!! example "Příklad"

    ```py
    from logic import *
    import time

    while True:
        print(light_level())
        time.sleep_ms(100)
    ```
