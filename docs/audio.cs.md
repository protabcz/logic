# Zvuky

Zvuky lze vydávat pomocí modulu `audio`. Dostupné jsou následující funkce:

## `#!py audio.demo()`

Přehraje ukázkovou písničku.

!!! example "Příklad"

    ```py
    from logic import *

    audio.demo()
    ```

## `#!py audio.note(height)`

Začne hrát notu dané výšky. Výška noty (`height`) je řetězec s názvem noty
a oktávou.

!!! example "Příklad"

    ```py
    from logic import *

    audio.note("C4")
    ```

## `#!py audio.note(height, ms)`

Přehraje notu dané výšky (`height`) a délky (`ms`). Délka noty je udaná
v milisekundách.

Funkce nečeká na skončení přehrávání noty. Program pokračuje dále, zatímco
se nota přehrává na pozadí.

## `#!py audio.stop()`

Zastaví přehrávání noty nebo melodie.

!!! example "Příklad"

    ```py
    from logic import *
    import time

    audio.note("C4")
    time.sleep(2)
    audio.stop()
    ```

## `#!py audio.melody(melody, tempo)`

Přehraje melodii. Parametr `melody` je seznam not, parametr `tempo` je délka
každé noty v milisekundách. Zápis výšky noty viz
[funkce `#!py audio.note()`](#audionoteheight).

!!! example "Příklad"

    ```py
    from logic import *

    audio.melody(["c4", "d4", "e4", "f4", "g4"])
    ```

Volitelně může být místo libovolné noty dvojice `#!py (výška, jmenovatel)`
nebo trojice `#!py (výška, čitatel, jmenovatel)`. To pak udává hodnotu
(délku) této konkrétní noty, např. `#!py ("c4", 4)` je nota čtvrťová.

!!! example "Příklad"

    ```py
    from logic import *

    audio.melody(["c4", ("d4", 2), ("e4", 3, 4), ("f4", 4), ("g4", 16)])
    ```

## `#!py audio.set_mode(mode)`

!!! note "Poznámka"

    Tato funkce není v českém prostředí potřeba.

Nastaví způsob, jakým jsou pojmenovány noty. Parametr `mode` může být
`#!py "german"` pro německé (a české) pojmenování not, nebo
`#!py "english"` pro anglické pojmenování not.

Toto nastavení se týká pouze pojmenování not (tedy význam noty B). Oktávy
lze vždy zadávat jak v klasické číselné notaci (C4), tak v Helmholtzově
notaci (c').

Výchozí hodnota je `#!py "german"`.
