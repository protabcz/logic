# Introduction

[Logic](https://logic.robotikabrno.cz/) is an educational board designed by
Robotárna Brno.

## Documentation

* [Display](display.md)
* [Sounds](audio.md)
* [Buttons](buttons.md)
* [Sensors](sensors.md)
* [Timer](timer.md)
