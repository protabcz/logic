# Úvod

[Logic](https://logic.robotikabrno.cz/) je výuková programovatelná hračka
navržená Robotárnou Brno.

## V čem psát programy

Pro psaní programů je doporučen [editor Mu](https://codewith.mu/).

Až si ho nainstalujete a spustíte, vyberte režim **ESP MicroPython**.

Logic má dva USB porty. Pro MicroPython je potřeba jej vždy připojovat do
toho označeného UART (levý při pohledu shora).

## První program

!!! example "Příklad"

    ```py
    from logic import *
    import time

    while True:
        display.set_pixel(4, 4, 'red')
        time.sleep(1)
        display.set_pixel(4, 4, 'green')
        time.sleep(1)
    ```

## Dokumentace

* [Displej](display/index.md)
* [Zvuky](audio.md)
* [Tlačítka](buttons.md)
* [Senzory](sensors.md)
* [Časovač](timer.md)

## Další informace

* [Dokumentace k MicroPythonu](https://docs.micropython.org/en/latest/esp32/quickref.html)
  (anglicky)
