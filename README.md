# Robotárna Logic board with MicroPython

[Logic](https://logic.robotikabrno.cz/) is an educational board designed by
Robotárna Brno. The original instructions are for the C programming
language; this repository provides MicroPython support.

Only the v2.0 revision of the board is currently supported.

## [Documentation](https://protabcz.gitlab.io/logic/)

## Tools and IDE

Install [esptool](https://github.com/espressif/esptool) and
[Mu](https://codewith.mu/). Both have packages in common distros and both
are available on Pypi.

* Fedora: `dnf install esptool mu`
* generic: `pip install esptool mu-editor`

## Flashing MicroPython

Connect the board to the 'UART' port and execute:

```
./flash.sh
```

By default, `/dev/ttyUSB0` is used for flashing. If you need a different
device, pass it as a parameter to `flash.sh`.

## Building the MicroPython firmware from scratch

Clone the MicroPython repository and add the Logic board:

```
git clone https://gitlab.com/protabcz/logic.git
git clone https://github.com/micropython/micropython.git
ln -s ../../../../logic/ROBOTARNA_LOGIC micropython/ports/esp32/boards/
```

Follow the [official instructions](https://github.com/micropython/micropython/tree/master/ports/esp32)
to clone and install ESP-IDF.

Then build MicroPython:

```
cd micropython/ports/esp32
make submodules
make BOARD=ROBOTARNA_LOGIC
```

The built firmware will be in `build-ROBOTARNA_LOGIC/firmware.bin`.
