from logic import *
import time

class Sprite:
    def __init__(self, level, x, y):
        # x, y is lower left corner of the sprite
        self.level = level
        self.x = x
        self.y = y
        self.width = 1
        self.height = 1
        self.erase_rect = None
        self.dirty = False
        self.visible = False
        self.to_be_removed = False
        self.init()

    def init(self):
        pass

    def pop_erase_rect(self):
        result = self.erase_rect
        if result is None:
            return None
        self.erase_rect = None
        if self.visible and result[0] == self.x and result[2] == self.y:
            self.dirty = True
            return None
        return result

    def get_dirty_rect(self):
        if not self.dirty:
            return None
        return (self.x, self.x + self.width, self.y, self.y + self.height)

    def cache_collision_rect(self):
        if not self.visible:
            self.collision_rect = None
            return
        self.collision_rect = (self.x, self.x + self.width, self.y, self.y + self.height)

    def is_within(self, xstart, xstop):
        return self.x + self.width > self.xstart and self.x < xstop

    def can_move(self, sx, sy):
        if self.x + sx < 0 or self.x + sx + self.width > self.level.width:
            return False
        if self.y + sy - self.height + 1 < 0 or self.y + sy >= 10:
            return False
        return True

    def move(self, sx, sy):
        if not self.can_move(sx, sy):
            return False
        if self.visible and self.erase_rect is None:
            self.erase_rect = (self.x, self.x + self.width, self.y, self.y + self.height)
        self.x += sx
        self.y += sy
        self.dirty = self.visible
        return True

    def show(self):
        self.visible = True
        self.dirty = True

    def hide(self):
        if self.erase_rect is None:
            self.erase_rect = (self.x, self.x + self.width, self.y, self.y + self.height)
        self.visible = False
        self.dirty = False

    def remove(self):
        self.hide()
        self.to_be_removed = True

    def _draw(self, xstart, xstop):
        pass

    def draw(self, xstart, xstop):
        if self.visible:
            local_xstart = max(0, xstart - self.x)
            local_xstop = min(self.width, xstop - self.x)
            if local_xstart < local_xstop:
                self._draw(local_xstart, local_xstop)
        self.dirty = False

    def update(self):
        pass

    def collision(self, other):
        pass


class Conveyor(Sprite):
    def init(self):
        self.phase = 0
        self.direction = 1
        self.show()

    def set_direction(self, direction):
        self.direction = direction

    def cache_collision_rect(self):
        self.collision_rect = None

    def update(self):
        self.phase += self.direction
        if self.phase >= 4:
            self.phase = 0
        elif self.phase < 0:
            self.phase = 3
        self.dirty = True

    def _draw(self, xstart, xstop):
        for x in range(xstart, xstop):
            c = COLOR_BELT2 if x % 4 == self.phase else COLOR_BELT1
            self.level.draw_pixel(self.x + x, self.y, c)


class Character(Sprite):
    def can_move(self, sx, sy):
        if not super().can_move(sx, sy):
            return False
        return all(not self.level.get_block(self.x + sx + x, self.y + sy - y) & SOLID
                   for x in range(self.width) for y in range(self.height))


class NPC(Character):
    def init(self):
        self.phase = 1
        self.direction = 1

    def update(self):
        below = self.level.get_block(self.x, self.y + 1)
        if not below & SOLID:
            self.move(0, 1)
            self.phase = 1
            return
        self.phase += 1
        if self.phase > 1:
            self.phase = 0
            return
        if not self.move(self.direction, 0):
            self.direction = -self.direction
            self.move(self.direction, 0)


class Bonus(NPC):
    def init(self):
        super().init()
        self.show()
        audio.note('g6', 100)

    def _draw(self, xstart, xstop):
        self.level.draw_pixel(self.x, self.y, COLOR_BONUS)

    def collision(self, other):
        if type(other) == Player:
            self.remove()
            audio.melody(('c6', 'e6'), 100)
            self.level.status.inc_life()


class Enemy(NPC):
    def init(self):
        super().init()
        self.show()

    def _draw(self, xstart, xstop):
        self.level.draw_pixel(self.x, self.y, COLOR_ENEMY)

    def collision(self, other):
        if type(other) == Player and not other.invincible:
            other.set_invincible()
            audio.note('c3', 500)
            return self.level.status.dec_life()


class Player(Character):
    def init(self):
        self.height = 2
        self.jumping = False
        self.invincible = 0
        self.show()

    def move(self, sx, sy):
        result = super().move(sx, sy)
        if result:
            self.level.ensure_visible(self.x, 3)
        return result

    def _draw(self, xstart, xstop):
        colset = self.invincible % 2
        self.level.draw_pixel(self.x, self.y - 1, COLOR_PLAYER_HEAD if colset == 0
                                                  else COLOR_PLAYER_HEAD2)
        self.level.draw_pixel(self.x, self.y, COLOR_PLAYER_BODY if colset == 0
                                              else COLOR_PLAYER_BODY2)

    def update(self):
        if self.invincible > 0:
            self.invincible -= 1
            self.dirty = True
        below = self.level.get_block(self.x, self.y + 1)
        above = self.level.get_block(self.x, self.y - 2)
        if buttons_a.up and not self.jumping and below & SOLID:
            self.jumping = 3
            if not above & SOLID:
                audio.note('g5', 50)
        if buttons_a.down and self.jumping:
            self.jumping = 0
        if self.jumping:
            if above == BONUS:
                self.level.set_block(self.x, self.y - 2, WALL)
                self.level.add_sprite(Bonus(self.level, self.x, self.y - 3))
            if self.move(0, -1):
                self.jumping -= 1
            else:
                self.jumping = 0
        elif not below & SOLID:
            self.move(0, 1)
        if below == BELT_RIGHT:
            self.move(1, 0)
        elif below == BELT_LEFT:
            self.move(-1, 0)
        if buttons_a.left:
            self.move(-1, 0)
        if buttons_a.right:
            self.move(1, 0)
        if self.y >= 9:
            return RESTART

    def set_invincible(self):
        self.invincible = 15
        self.dirty = True


RESTART = micropython.const(1)
GAME_OVER = micropython.const(2)

SOLID = micropython.const(0x80)
WALL = micropython.const(SOLID | 0x01)
BELT_RIGHT = micropython.const(SOLID | 0x02)
BELT_LEFT = micropython.const(SOLID | 0x03)
BONUS = micropython.const(SOLID | 0x04)

COLOR_WALL = Color.translate('aaa')
COLOR_BONUS = Color.translate('0a0')
COLOR_ENEMY = Color.translate('a00')
COLOR_PLAYER_HEAD = Color.translate('a0a')
COLOR_PLAYER_BODY = Color.translate('00a')
COLOR_PLAYER_HEAD2 = Color.translate('404')
COLOR_PLAYER_BODY2 = Color.translate('004')
COLOR_BELT1 = Color.translate('055')
COLOR_BELT2 = Color.translate('0aa')

COLOR_STATUS_LIFE = Color.translate('070')


class Level:
    colors = { WALL: COLOR_WALL,
               BONUS: COLOR_BONUS,
             }

    def __init__(self, status, filename):
        self.status = status
        self.sprites = []
        with open(filename) as f:
            for y, data in enumerate(f):
                if y == 0:
                    self.width = len(data) - 1
                    self.level = bytearray(self.width * 10)
                last = None
                for x, p in enumerate(data):
                    if p == '#':
                        self.level[y * self.width + x] = WALL
                    elif p == '*':
                        self.level[y * self.width + x] = BONUS
                    elif p in ('>', '<'):
                        if last == p:
                            self.sprites[-1].width += 1
                        else:
                            self.sprites.append(Conveyor(self, x, y))
                            self.sprites[-1].set_direction(1 if p == '>' else -1)
                        self.level[y * self.width + x] = BELT_RIGHT if p == '>' else BELT_LEFT
                    elif p == '@':
                        self.sprites.append(Player(self, x, y))
                    elif p == '!':
                        self.sprites.append(Enemy(self, x, y))
                    last = p
        self.new_sprites = []
        self.wx = 0
        self.dirty = True
        self.exposed_area = None
        self.jumping = False

    def draw_pixel(self, x, y, color):
        x -= self.wx
        if x < 0 or x >= 10 or y < 0 or y >= 10:
            return
        display.image.set_pixel(x, y, color)
        self.dirty = True

    def add_exposed_area(self, xrange):
        if xrange is None:
            return
        if self.exposed_area is None:
            self.exposed_area = xrange
            return
        self.exposed_area = (min(self.exposed_area[0], xrange[0]),
                             max(self.exposed_area[1], xrange[1]))

    def draw_columns(self, xrange):
        for y in range(10):
            for x in range(*xrange):
                block = self.level[y * self.width + x + self.wx]
                display.image.set_pixel(x, y, self.colors.get(block, 0))
        self.dirty = True
        self.add_exposed_area(xrange)

    def xrange_to_window(self, xrange):
        if xrange is None:
            return None
        xstart = max(0, xrange[0] - self.wx)
        xstop = min(10, xrange[1] - self.wx)
        if xstart < xstop:
            return (xstart, xstop)
        return None

    def redraw_sprites(self):
        for sprite in self.sprites:
            r = self.xrange_to_window(sprite.pop_erase_rect())
            if r:
                self.draw_columns(r)
            self.add_exposed_area(self.xrange_to_window(sprite.get_dirty_rect()))
        if self.exposed_area is None:
            return
        # redraw all sprites in exposed area to handle overlaps
        # between the sprites
        for sprite in self.sprites:
            sprite.draw(self.exposed_area[0] + self.wx, self.exposed_area[1] + self.wx)
        self.exposed_area = None

    def redraw(self):
        self.draw_columns((0, 10))
        self.redraw_sprites()

    def commit(self):
        if self.dirty:
            display.commit()
            self.dirty = False

    def scroll(self, step):
        if step > 0:
            step = min(step, self.wx)
        else:
            step = -min(-step, self.width - self.wx - 10)
        if step == 0:
            return
        display.image.scroll(step, 0)
        self.wx -= step
        if step > 0:
            self.draw_columns((0, step))
        else:
            self.draw_columns((10 + step, 10))

    def ensure_visible(self, x, margin):
        if self.wx + margin > x:
            self.scroll(self.wx + margin - x)
        elif self.wx + 9 - margin < x:
            self.scroll(self.wx + 9 - margin - x)

    def start_effect(self):
        self.wx = self.width - 10
        self.redraw()
        self.commit()
        while self.wx > 0:
            time.sleep_ms(20)
            self.scroll(1)
            self.redraw_sprites()
            self.commit()

    def get_block(self, x, y):
        if x < 0 or x >= self.width or y < 0 or y >= 10:
            return WALL
        return self.level[y * self.width + x]

    def set_block(self, x, y, block):
        if x < 0 or x >= self.width or y < 0 or y >= 10:
            return
        self.level[y * self.width + x] = block
        x -= self.wx
        if x >= 0 and x < 10:
            self.draw_columns((x, x + 1))

    def add_sprite(self, sprite):
        self.new_sprites.append(sprite)

    def overlaps(self, rect1, rect2):
        return (((rect1[0] >= rect2[0] and rect1[0] < rect2[1]) or
                 (rect2[0] >= rect1[0] and rect2[0] < rect1[1])) and
                ((rect1[2] >= rect2[2] and rect1[2] < rect2[3]) or
                 (rect2[2] >= rect1[2] and rect2[2] < rect1[3])))

    def loop(self):
        buttons_a.clear()
        result = 0
        while not result:
            time.sleep_ms(100)
            # update
            for sprite in self.sprites:
                result = max(sprite.update() or 0, result)
            # new sprites
            if self.new_sprites:
                self.sprites.extend(self.new_sprites)
                self.new_sprites = []
            # collisions
            for sprite in self.sprites:
                sprite.cache_collision_rect()
            for i, sprite in enumerate(self.sprites):
                rect1 = sprite.collision_rect
                if not rect1:
                    continue
                for j in range(i + 1, len(self.sprites)):
                    rect2 = self.sprites[j].collision_rect
                    if not rect2:
                        continue
                    if self.overlaps(rect1, rect2):
                        result = max(sprite.collision(self.sprites[j]) or 0, result)
                        result = max(self.sprites[j].collision(sprite) or 0, result)
            # draw
            self.redraw_sprites()
            self.commit()
            # removed sprites
            for i in range(len(self.sprites) - 1, -1, -1):
                if self.sprites[i].to_be_removed:
                    del self.sprites[i]
        return result


class Status:
    def __init__(self):
        self.lives = 3
        self.redraw()

    def redraw(self):
        for i in range(5):
            display.status.image.set_pixel(i, 0, COLOR_STATUS_LIFE if i < self.lives else 0)
        display.status.commit()

    def dec_life(self):
        if self.lives == 0:
            return GAME_OVER
        self.lives -= 1
        self.redraw()
        return None

    def inc_life(self):
        if self.lives < 5:
            self.lives += 1
            self.redraw()


status = Status()
while True:
    level = Level(status, 'level1.dat')
    level.start_effect()
    result = level.loop()
    if result == RESTART:
        result = status.dec_life()
        if not result:
            audio.melody((('g3', 2), 'c3'), 500)
    if result == GAME_OVER:
        audio.melody((('g3', 2), ('e3', 2), ('c3', 2), 'g2'), 500)
        # TODO
