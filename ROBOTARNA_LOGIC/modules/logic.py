# SPDX-License-Identifier: MIT
# Drivers and utilities for Robotarna Logic
# Copyright (c) 2023 Jiri Benc
# the original NeoPixel driver:
#   Copyright (c) 2016 Damien P. George, 2021 Jim Mussared (MIT license)

import machine
import math
import micropython
import struct
import time


class Color:
    names = { 'aliceblue': 0xf0f8ff, 'antiquewhite': 0xfaebd7, 'aqua': 0x00ffff,
    'aquamarine': 0x7fffd4, 'azure': 0xf0ffff, 'beige': 0xf5f5dc, 'bisque': 0xffe4c4,
    'black': 0x000000, 'blanchedalmond': 0xffebcd, 'blue': 0x0000ff, 'blueviolet': 0x8a2be2,
    'brown': 0xa52a2a, 'burlywood': 0xdeb887, 'cadetblue': 0x5f9ea0, 'chartreuse': 0x7fff00,
    'chocolate': 0xd2691e, 'coral': 0xff7f50, 'cornflowerblue': 0x6495ed,
    'cornsilk': 0xfff8dc, 'crimson': 0xdc143c, 'cyan': 0x00ffff, 'darkblue': 0x00008b,
    'darkcyan': 0x008b8b, 'darkgoldenrod': 0xb8860b, 'darkgrey': 0xa9a9a9,
    'darkgreen': 0x006400, 'darkkhaki': 0xbdb76b, 'darkmagenta': 0x8b008b,
    'darkolivegreen': 0x556b2f, 'darkorange': 0xff8c00, 'darkorchid': 0x9932cc,
    'darkred': 0x8b0000, 'darksalmon': 0xe9967a, 'darkseagreen': 0x8fbc8f,
    'darkslateblue': 0x483d8b, 'darkslategrey': 0x2f4f4f, 'darkturquoise': 0x00ced1,
    'darkviolet': 0x9400d3, 'deeppink': 0xff1493, 'deepskyblue': 0x00bfff,
    'dimgray': 0x696969, 'dodgerblue': 0x1e90ff, 'firebrick': 0xb22222,
    'floralwhite': 0xfffaf0, 'forestgreen': 0x228b22, 'fuchsia': 0xff00ff,
    'gainsboro': 0xdcdcdc, 'ghostwhite': 0xf8f8ff, 'gold': 0xffd700, 'goldenrod': 0xdaa520,
    'grey': 0x808080, 'green': 0x008000, 'greenyellow': 0xadff2f, 'honeydew': 0xf0fff0,
    'hotpink': 0xff69b4, 'indianred': 0xcd5c5c, 'indigo': 0x4b0082, 'ivory': 0xfffff0,
    'khaki': 0xf0e68c, 'lavender': 0xe6e6fa, 'lavenderblush': 0xfff0f5,
    'lawngreen': 0x7cfc00, 'lemonchiffon': 0xfffacd, 'lightblue': 0xadd8e6,
    'lightcoral': 0xf08080, 'lightcyan': 0xe0ffff, 'lightgoldenrodyellow': 0xfafad2,
    'lightgrey': 0xd3d3d3, 'lightgreen': 0x90ee90, 'lightpink': 0xffb6c1,
    'lightsalmon': 0xffa07a, 'lightseagreen': 0x20b2aa, 'lightskyblue': 0x87cefa,
    'lightslategrey': 0x778899, 'lightsteelblue': 0xb0c4de, 'lightyellow': 0xffffe0,
    'lime': 0x00ff00, 'limegreen': 0x32cd32, 'linen': 0xfaf0e6, 'magenta': 0xff00ff,
    'maroon': 0x800000, 'mediumaquamarine': 0x66cdaa, 'mediumblue': 0x0000cd,
    'mediumorchid': 0xba55d3, 'mediumpurple': 0x9370d8, 'mediumseagreen': 0x3cb371,
    'mediumslateblue': 0x7b68ee, 'mediumspringgreen': 0x00fa9a, 'mediumturquoise': 0x48d1cc,
    'mediumvioletred': 0xc71585, 'midnightblue': 0x191970, 'mintcream': 0xf5fffa,
    'mistyrose': 0xffe4e1, 'moccasin': 0xffe4b5, 'navajowhite': 0xffdead, 'navy': 0x000080,
    'oldlace': 0xfdf5e6, 'olive': 0x808000, 'olivedrab': 0x6b8e23, 'orange': 0xffa500,
    'orangered': 0xff4500, 'orchid': 0xda70d6, 'palegoldenrod': 0xeee8aa,
    'palegreen': 0x98fb98, 'paleturquoise': 0xafeeee, 'palevioletred': 0xd87093,
    'papayawhip': 0xffefd5, 'peachpuff': 0xffdab9, 'peru': 0xcd853f, 'pink': 0xffc0cb,
    'plum': 0xdda0dd, 'powderblue': 0xb0e0e6, 'purple': 0x800080, 'red': 0xff0000,
    'rosybrown': 0xbc8f8f, 'royalblue': 0x4169e1, 'saddlebrown': 0x8b4513,
    'salmon': 0xfa8072, 'sandybrown': 0xf4a460, 'seagreen': 0x2e8b57, 'seashell': 0xfff5ee,
    'sienna': 0xa0522d, 'silver': 0xc0c0c0, 'skyblue': 0x87ceeb, 'slateblue': 0x6a5acd,
    'slategrey': 0x708090, 'snow': 0xfffafa, 'springgreen': 0x00ff7f, 'steelblue': 0x4682b4,
    'tan': 0xd2b48c, 'teal': 0x008080, 'thistle': 0xd8bfd8, 'tomato': 0xff6347,
    'turquoise': 0x40e0d0, 'violet': 0xee82ee, 'wheat': 0xf5deb3, 'white': 0xffffff,
    'whitesmoke': 0xf5f5f5, 'yellow': 0xffff00, 'yellowgreen': 0x9acd32 }

    @staticmethod
    def translate(color):
        if isinstance(color, int):
            return color
        if isinstance(color, str):
            result = Color.names.get(color.lower(), None)
            if result is not None:
                return result
            if color and color[0] == '#':
                color = color[1:]
            if len(color) == 3:
                color = color[0] * 2 + color[1] * 2 + color[2] * 2
            elif len(color) != 6:
                raise ValueError('Invalid color name')
            return int(color, 16)
        # assume color is iterable with 3 fields RGB
        result = 0
        for i in color:
            if isinstance(i, float):
                i = int(i * 255)
            if not isinstance(i, int) or i < 0 or i > 255:
                raise ValueError(f'Invalid color component: {i}')
            result = result << 8 | i
        return result

    @staticmethod
    def normalize(component):
        return (component * component + 254) // (255 * 10)


class Image:
    def __init__(self, width=10, height=10):
        self.width = width
        self.height = height
        self.buf = bytearray(width * height * 3)

    def set_pixel(self, x, y, color):
        offset = (y * self.width + x) * 3
        color = Color.translate(color)
        # NeoPixel is GRB
        self.buf[offset] = Color.normalize((color >> 8) & 0xff)
        self.buf[offset + 1] = Color.normalize((color >> 16) & 0xff)
        self.buf[offset + 2] = Color.normalize(color & 0xff)

    def clear(self):
        self.buf = bytearray(self.width * self.height * 3)

    def scroll(self, x_step, y_step):
        v = memoryview(self.buf)
        if y_step <= 0:
            ygen = range(-y_step, self.height)
        else:
            ygen = range(self.height - y_step - 1, -1, -1)
        for y in ygen:
            src = y * self.width
            dst = (y + y_step) * self.width
            if x_step <= 0:
                v[dst*3:(dst+self.width+x_step)*3] = v[(src-x_step)*3:(src+self.width)*3]
            else:
                v[(dst+x_step)*3:(dst+self.width)*3] = v[src*3:(src+self.width-x_step)*3]

    def load(self, path):
        with open(path, 'rb') as f:
            header = struct.unpack('<BBIHHI', f.read(14))
            if header[0] != 0x42 or header[1] != 0x4d:
                raise TypeError('Not a BMP file')
            dib_length = header[5] - 14
            if dib_length < 40:
                raise TypeError('Unsupported (old) BMP file')
            dib = struct.unpack('<IiiHHIIiiII', f.read(40))
            if dib[1] != self.width or dib[2] != self.height:
                raise TypeError('Image dimensions do not match the BMP')
            if dib[3] != 1 or dib[4] != 24:
                raise TypeError('Unsupported color depth')
            if dib[5] != 0:
                raise TypeError('Compression is not supported')
            f.read(dib_length - 40)
            row_size = (self.width * 3 + 3) & ~3
            for y in range(self.height - 1, -1, -1):
                row = f.read(row_size)
                offset = y * self.width * 3
                for x in range(self.width):
                    # NeoPixel is GRB, BMP is BGR
                    self.buf[offset + x * 3] = Color.normalize(row[x * 3 + 1])
                    self.buf[offset + x * 3 + 1] = Color.normalize(row[x * 3 + 2])
                    self.buf[offset + x * 3 + 2] = Color.normalize(row[x * 3])


class NeoPixel:
    def __init__(self, pin, width, height):
        self.image = Image(width, height)
        self.pin = pin
        self.pin.init(pin.OUT)
        # hardcode timing to 800kHz
        self.timing = (400, 850, 800, 450)

    def set_pixel(self, x, y, color):
        if x < 0 or x >= self.image.width:
            raise IndexError(f'Invalid x coordinate {x}')
        if y < 0 or y >= self.image.height:
            raise IndexError(f'Invalid y coordinate {y}')
        self.image.set_pixel(x, y, color)
        self.commit()

    def clear(self):
        self.image.clear()
        self.commit()

    def show(self, image):
        if (image.width != self.image.width or image.height != self.image.height):
            raise ValueError('The image dimensions do not match the display')
        self.image.buf[:] = image.buf
        self.commit()

    def load(self, path):
        self.image.load(path)
        self.commit()

    def commit(self):
        machine.bitstream(self.pin, 0, self.timing, self.image.buf)


class NeoPixelRow(NeoPixel):
    def __init__(self, pin, width):
        super().__init__(pin, width, 1)

    def set_pixel(self, x, color):
        super().set_pixel(x, 0, color)


class Display(NeoPixel):
    def __init__(self):
        self._pwr = machine.Pin(0, machine.Pin.OUT)
        super().__init__(machine.Pin(45, machine.Pin.OUT), 10, 10)
        self.status = NeoPixelRow(machine.Pin(46, machine.Pin.OUT), 5)
        self.on()

    def off(self):
        self._pwr.value(0)

    def on(self):
        self._pwr.value(1)
        self.commit()
        self.status.commit()


class Alarm:
    def __init__(self, timer):
        self.timer = timer

    def stop(self):
        self.timer.stop(self)


class Timer:
    def __init__(self, id=0):
        self._timer = machine.Timer(id)
        self._alarms = []   # tuple of (deadline, callback, Alarm object)

    def create(self, ms, callback):
        alarm = Alarm(self)
        self._queue_command(0, alarm, ms, callback)
        return alarm

    def stop(self, alarm):
        self._queue_command(1, alarm)

    def _queue_command(self, *args):
        micropython.schedule(self._exec, args)

    def _irq_handler(self, timer):
        micropython.schedule(self._exec, None)

    def _exec(self, args):
        reschedule = False
        now = time.ticks_ms()
        # check if alarm was triggered
        while self._alarms:
            period = time.ticks_diff(self._alarms[0][0], now)
            if period > 0:
                break
            # triggered; call the callback
            self._alarms[0][1](self._alarms[0][2])
            del self._alarms[0]
            reschedule = True
        if args and args[0] == 0:
            # add alarm
            when = time.ticks_add(now, args[2])
            i = 0
            while i < len(self._alarms) and time.ticks_diff(self._alarms[i][0], when) <= 0:
                i += 1
            self._alarms.insert(i, (when, args[3], args[1]))
            if i == 0:
                reschedule = True
        elif args and args[0] == 1:
            # remove alarm
            for i, alarm in enumerate(self._alarms):
                if alarm[2] == args[1]:
                    del self._alarms[i]
                    if i == 0:
                        reschedule = True
                    break
        # reschedule the timer if needed
        if not reschedule:
            return
        if self._timer:
            self._timer.deinit()
        if self._alarms:
            period = max(time.ticks_diff(self._alarms[0][0], now), 0)
            self._timer.init(mode=machine.Timer.ONE_SHOT, period=period,
                             callback=self._irq_handler)


class Buttons:
    names = { 'up': 0, 'left': 1, 'enter': 2, 'right': 3, 'down': 4 }
    _debounce_interval = 20

    def __init__(self, pin_ids):
        self._pin_mapping = {}
        self._pins = []
        self.state = []
        for i, pin_id in enumerate(pin_ids):
            pin = machine.Pin(pin_id, machine.Pin.IN, machine.Pin.PULL_UP)
            self._pins.append(pin)
            self._pin_mapping[pin] = i
            self.state.append(not pin())
        self._debounced_state = self.state.copy()
        self._state_pressed = [False] * len(pin_ids)
        self._state_time = [-1] * len(pin_ids)
        self._debounce_alarm = None
        for pin in self._pins:
            pin.irq(self._irq_handler)

    def _irq_handler(self, pin):
        index = self._pin_mapping[pin]
        self.state[index] = not pin()
        self._state_time[index] = time.ticks_ms()
        micropython.schedule(self._debounce, None)

    def _debounce(self, _):
        now = time.ticks_ms()
        if self._debounce_alarm:
            self._debounce_alarm.stop()
            self._debounce_alarm = None
        next_call = 1000
        for i, (t, state) in enumerate(zip(self._state_time, self.state)):
            if t < 0:
                continue
            diff = self._debounce_interval - time.ticks_diff(now, t)
            if diff <= 0:
                if state != self._debounced_state[i]:
                    self._debounced_state[i] = state
                    if state:
                        self._state_pressed[i] = True
                self._state_time[i] = -1
            elif diff < next_call:
                next_call = diff
        if next_call < 1000:
            self._debounce_alarm = timer.create(next_call, self._debounce)

    def is_pressed(self, button):
        try:
            return self._debounced_state[self.names[button]]
        except KeyError:
            raise ValueError(f'Invalid button name: {button}')

    def was_pressed(self, button):
        try:
            val = self.names[button]
        except KeyError:
            raise ValueError(f'Invalid button name: {button}')
        result = self._debounced_state[val] or self._state_pressed[val]
        self._state_pressed[val] = False
        return result

    def clear(self):
        for i in range(len(self._state_pressed)):
            self._state_pressed[i] = False

    def __getattr__(self, name):
        try:
            return self.was_pressed(name)
        except ValueError:
            raise AttributeError(f'Invalid button name: {name}')


class LightSensor:
    def __init__(self):
        self._adc = machine.ADC(machine.Pin(2))

    def __call__(self):
        return self._adc.read_u16() * 100 // 65535


class Audio:
    def __init__(self):
        self._pwm = machine.PWM(machine.Pin(21))
        self._pwm.deinit()
        self._alarm = None
        self._german = True

    def set_mode(self, mode):
        if mode == 'english':
            self._german = False
        elif mode == 'german':
            self._german = True
        else:
            raise ValueError(f'Invalid mode: {mode}')

    def _soft_stop(self):
        if self._alarm:
            self._alarm.stop()
            self._alarm = None

    def _prescheduled_next_note(self, alarm):
        if alarm != self._alarm:
            return
        self._alarm = timer.create(max(1, self._tempo // 500), self._scheduled_next_note)

    def _scheduled_next_note(self, alarm):
        if alarm != self._alarm:
            return
        self._alarm = None
        self._next_note()

    def _next_note(self):
        try:
            height = self._melody[self._melody_pos]
            self._melody_pos += 1
            if type(height) != str:
                if len(height) == 2:
                    height, denominator = height
                    numerator = 1
                else:
                    height, numerator, denominator = height
            else:
                numerator = 1
                denominator = 1
        except (IndexError, TypeError, ValueError):
            self._stop()
            return
        letter = height[0].lower()
        if letter == '-':
            self._soft_stop()
            self._pwm.deinit()
            if self._tempo:
                self._alarm = timer.create(self._tempo * numerator // denominator,
                                           self._scheduled_next_note)
            return
        if letter < 'a' or letter > 'h' or (not self._german and letter == 'h'):
            raise ValueError(f'Invalid note: {height}')
        if self._german and letter == 'b':
            semi = 10
        elif letter <= 'b':
            semi = (ord(letter) - ord('a')) * 2 + 9
        elif letter <= 'e':
            semi = (ord(letter) - ord('c')) * 2
        elif letter <= 'g':
            semi = (ord(letter) - ord('f')) * 2 + 5
        elif letter == 'h':
            semi = 11
        pos = 1
        while pos < len(height):
            if height[pos] == '#':
                semi += 1
            elif height[pos] == 'b':
                semi -= 1
            else:
                break
            pos += 1
        if pos < len(height):
            if height[pos] in (',', "'"):
                # Helmholtz
                i = pos + 1
                while i < len(height):
                    if height[i] != height[pos]:
                        raise ValueError(f'Invalid note: {height}')
                if height[pos] == ',':
                    if height[0].islower():
                        raise ValueError(f'Invalid note: {height}')
                    octave = 2 - (len(height) - pos)
                else:
                    if height[0].isupper():
                        raise ValueError(f'Invalid note: {height}')
                    octave = 3 + len(height) - pos
            else:
                # scientific
                try:
                    octave = int(height[pos:])
                except ValueError:
                    raise ValueError(f'Invalid note: {height}')
        else:
            # no explicit octave specified, assume Helmholtz
            octave = 3 if height[0].islower() else 2
        # the reference note is A4
        shift = (octave - 4) * 12 + semi - 9
        if shift < -40:
            raise ValueError(f'Too low note: {height}')
        if shift > 50:
            raise ValueError(f'Too high note: {height}')
        hz = round(math.pow(2, shift / 12) * 440)
        self._soft_stop()
        self._pwm.init(freq=hz, duty_u16=32767)
        if self._tempo:
            ms = max(0, self._tempo * numerator // denominator - max(1, self._tempo // 500))
            self._alarm = timer.create(ms, self._prescheduled_next_note)

    def _set_melody(self, data):
        self._stop()
        self._melody = data[0]
        self._melody_pos = 0
        self._tempo = data[1]
        self._next_note()

    def note(self, height, ms=None):
        micropython.schedule(self._set_melody, ([height], ms))

    def melody(self, melody, tempo=50):
        if isinstance(melody, str):
            raise ValueError('Melody must an iterable.')
        micropython.schedule(self._set_melody, (melody, tempo))

    def _stop(self, data=None):
        self._soft_stop()
        self._melody = None
        self._melody_pos = None
        self._pwm.deinit()

    def stop(self):
        micropython.schedule(self._stop, None)

    def demo(self):
        self.melody((('f4', 8), ('f4', 8), ('e4', 8), ('c4', 8),
                     ('f4', 8), ('f4', 8), ('e4', 8), ('c4', 8),
                     ('d4', 8), ('d4', 8), ('e4', 8), ('e4', 8),
                     ('f4', 2), ('-', 2),
                     ('f4', 8), ('a4', 8), ('a4', 8), ('c5', 8),
                     ('c5', 8), ('a#4', 8), ('g4', 4),
                     ('f4', 8), ('a4', 8), ('a4', 8), ('c5', 8),
                     ('c5', 8), ('a#4', 8), ('g4', 4),
                     ('f4', 8), ('f4', 8), ('e4', 8), ('c4', 8),
                     ('f4', 8), ('f4', 8), ('e4', 8), ('c4', 8),
                     ('d4', 8), ('d4', 8), ('e4', 8), ('e4', 8),
                     ('f4', 2)),
                    2000)


timer = Timer()
buttons_a = Buttons((47, 1, 5, 6, 7))
buttons_b = Buttons((8, 10, 9, 4, 14))
light_level = LightSensor()
display = Display()
audio = Audio()

__all__ = ['timer', 'buttons_a', 'buttons_b', 'display', 'audio']
